# /OpenApi/getGameResult

回傳彩種的獎期資訊與號碼資訊.

## Endpoint definition

`/OpenApi/getGameResult`

## HTTP method

<span class="label label-primary">GET</span>

## Parameters

| Parameter | Description | Data Type |
|-----------|------|-----|
| game_value | *Optional*. 彩種名稱. | string |
| cycle_value | *Optional*. 獎期.| integer |

## Sample request

```
curl -I -X GET "http://kaijiang.goodgaming.com/index.php?s=/OpenApi/getGameResult/game_value/jssyxw/cycle_value/18050711"
```

## Sample response

```json
{
  "rows": 58,
  "remain": "5524hrs",
  "data": [
    {
      "code": "afgh90",
      "expect": "201711150394",
      "opencode": "6,0,7,1,4",
      "opentime": "2018-01-10 16:29:58"
    },
    {
      "code": "ah11x5",
      "expect": "2017111555",
      "opencode": "07,02,09,06,05",
      "opentime": "2018-01-10 16:29:58"
    },
    {
      "code": "ahk3",
      "expect": "20180620010",
      "opencode": "3,4,6",
      "opentime": "2018-06-20 10:20:35"
    },
    {
      "code": "ahks",
      "expect": "20180620009",
      "opencode": "1,4,6",
      "opentime": "2018-06-20 10:12:00"
    },
    {
      "code": "bjkl8",
      "expect": "894394",
      "opencode": "03,05,10,25,27,43,47,50,51,52,53,54,55,57,58,64,66,67,74,76+01",
      "opentime": "2018-06-20 10:20:25"
    },
    {
      "code": "bjpk10",
      "expect": "688415",
      "opencode": "06,03,10,01,05,07,04,09,02,08",
      "opentime": "2018-06-20 10:18:06"
    },
    {
      "code": "cqklsf",
      "expect": "20171115060",
      "opencode": "13,11,16,19,14,07,03,18",
      "opentime": "2018-01-10 16:29:58"
    },
    {
      "code": "cqssc",
      "expect": "20180620026",
      "opencode": "4,6,8,1,3",
      "opentime": "2018-06-20 10:21:11"
    },
    {
      "code": "fc3d",
      "expect": "2018163",
      "opencode": "8,6,9",
      "opentime": "2018-06-19 21:38:25"
    },
    {
      "code": "gd11x5",
      "expect": "2018062007",
      "opencode": "11,02,01,05,10",
      "opentime": "2018-06-20 10:11:54"
    },
    {
      "code": "gdklsf",
      "expect": "2018062007",
      "opencode": "14,11,05,12,13,07,03,08",
      "opentime": "2018-06-20 10:13:01"
    },
    {
      "code": "gxk3",
      "expect": "20171115050",
      "opencode": "2,4,5",
      "opentime": "2018-01-10 16:29:58"
    },
    {
      "code": "gxklsf",
      "expect": "201816405",
      "opencode": "05,01,12,17,13",
      "opentime": "2018-06-20 10:17:00"
    },
    {
      "code": "gxks",
      "expect": "180620005",
      "opencode": "3,4,5",
      "opentime": "2018-06-20 10:17:53"
    },
    {
      "code": "hgklb",
      "expect": "1889199",
      "opencode": "04,06,13,16,19,32,41,45,46,47,55,59,61,65,66,67,70,71,72,73",
      "opentime": "2018-01-10 16:29:58"
    },
    {
      "code": "hk6",
      "expect": "2018067",
      "opencode": "07,42,02,31,21,33,24",
      "opentime": "2018-06-19 21:36:22"
    },
    {
      "code": "hlj11x5",
      "expect": "2018061888",
      "opencode": "05,02,07,04,01",
      "opentime": "2018-06-18 22:37:43"
    },
    {
      "code": "hljssc",
      "expect": "0269177",
      "opencode": "7,2,8,2,5",
      "opentime": "2018-06-20 10:11:57"
    },
    {
      "code": "hnklsf",
      "expect": "20180620008",
      "opencode": "17,06,01,04,13,15,03,11",
      "opentime": "2018-06-20 10:20:25"
    },
    {
      "code": "hnsyxw",
      "expect": "18062012",
      "opencode": "10,07,02,03,05",
      "opentime": "2018-06-20 10:17:07"
    },
    {
      "code": "hubk3",
      "expect": "20180620007",
      "opencode": "1,3,6",
      "opentime": "2018-06-20 10:12:25"
    },
    {
      "code": "hunklsf",
      "expect": "20180612055",
      "opencode": "16,06,04,07,09,08,05,19",
      "opentime": "2018-06-12 18:10:25"
    },
    {
      "code": "inffc5",
      "expect": "20171115214",
      "opencode": "2,3,5,6,0",
      "opentime": "2018-01-10 16:29:58"
    },
    {
      "code": "jlk3",
      "expect": "20180620013",
      "opencode": "5,6,6",
      "opentime": "2018-06-20 10:20:26"
    },
    {
      "code": "jndklb",
      "expect": "2297420",
      "opencode": "02,05,07,08,18,21,27,36,41,46,47,49,54,62,64,67,72,73,75,76",
      "opentime": "2018-06-20 10:18:58"
    },
    {
      "code": "jpkeno",
      "expect": "20171005519",
      "opencode": "04,10,12,16,18,19,20,21,31,36,37,42,43,48,51,52,61,67,68,71",
      "opentime": "2018-01-10 16:29:58"
    },
    {
      "code": "js11x5",
      "expect": "2018062011",
      "opencode": "05,07,08,04,02",
      "opentime": "2018-06-20 10:17:05"
    },
    {
      "code": "jsk3",
      "expect": "180620011",
      "opencode": "1,2,5",
      "opentime": "2018-06-20 10:20:34"
    },
    {
      "code": "jssyxw",
      "expect": "18062011",
      "opencode": "05,07,08,04,02",
      "opentime": "2018-06-20 10:17:58"
    },
    {
      "code": "jx11x5",
      "expect": "2018062007",
      "opencode": "10,08,07,05,02",
      "opentime": "2018-06-20 10:12:20"
    },
    {
      "code": "ln11x5",
      "expect": "2017111555",
      "opencode": "11,07,01,02,03",
      "opentime": "2018-01-10 16:29:58"
    },
    {
      "code": "nmgk3",
      "expect": "20171115048",
      "opencode": "1,4,6",
      "opentime": "2018-01-10 16:29:58"
    },
    {
      "code": "p3p5",
      "expect": "2018163",
      "opencode": "5,7,0,3,5",
      "opentime": "2018-06-19 20:44:14"
    },
    {
      "code": "phkeno",
      "expect": "20171115674",
      "opencode": "38,78,20,18,80,77,15,51,11,28,35,79,41,58,12,55,22,02,47,31",
      "opentime": "2018-01-10 16:29:58"
    },
    {
      "code": "phkeno2",
      "expect": "20171115506",
      "opencode": "31,80,23,04,22,49,33,74,46,62,45,50,75,68,67,09,43,47,54,37",
      "opentime": "2018-01-10 16:29:58"
    },
    {
      "code": "pl3",
      "expect": "2017311",
      "opencode": "9,0,0",
      "opentime": "2018-01-10 16:29:58"
    },
    {
      "code": "pl5",
      "expect": "2018163",
      "opencode": "5,7,0,3,5",
      "opentime": "2018-06-19 20:37:35"
    },
    {
      "code": "pls",
      "expect": "2018163",
      "opencode": "5,7,0",
      "opentime": "2018-06-19 20:42:08"
    },
    {
      "code": "qqssc",
      "expect": "201803260572",
      "opencode": "247746163",
      "opentime": "2018-03-26 09:32:31"
    },
    {
      "code": "sd11x5",
      "expect": "2018062011",
      "opencode": "03,07,10,09,11",
      "opentime": "2018-06-20 10:17:53"
    },
    {
      "code": "sdsjh",
      "expect": "2018080",
      "opencode": "1,7,9",
      "opentime": "2018-03-28 18:10:38"
    },
    {
      "code": "sgnkl8",
      "expect": "2849263",
      "opencode": "02,04,09,14,17,20,24,27,36,38,40,44,59,63,67,70,76,77,78,80",
      "opentime": "2018-01-10 16:29:58"
    },
    {
      "code": "sh11x5",
      "expect": "2018062009",
      "opencode": "07,02,01,06,03",
      "opentime": "2018-06-20 10:21:05"
    },
    {
      "code": "shssl",
      "expect": "2018061923",
      "opencode": "9,4,8",
      "opentime": "2018-06-19 21:36:21"
    },
    {
      "code": "ssq",
      "expect": "2017134",
      "opencode": "04,05,11,14,28,32+04",
      "opentime": "2018-01-10 16:29:58"
    },
    {
      "code": "tjssc",
      "expect": "20180620008",
      "opencode": "2,3,5,6,3",
      "opentime": "2018-06-20 10:20:26"
    },
    {
      "code": "twbg",
      "expect": "107034550",
      "opencode": "01,02,06,10,14,15,17,27,30,37,39,42,50,56,57,62,65,67,71,75,17",
      "opentime": "2018-06-20 10:20:53"
    },
    {
      "code": "tx2fc",
      "expect": "20180620310",
      "opencode": "279166739",
      "opentime": "2018-06-20 10:20:02"
    },
    {
      "code": "txffc",
      "expect": "201806200621",
      "opencode": "279214666",
      "opentime": "2018-06-20 10:21:02"
    },
    {
      "code": "uspball45",
      "expect": "201711151429",
      "opencode": "01,08,15,19,23,25,32,37,40,46,52,53,54,56,59,62,67,73,75,77",
      "opentime": "2018-01-10 16:29:58"
    },
    {
      "code": "viffc5",
      "expect": "20171115214",
      "opencode": "8,4,6,7,3",
      "opentime": "2018-01-10 16:29:58"
    },
    {
      "code": "xdjklb",
      "expect": "20180620413",
      "opencode": "02,06,07,17,19,27,33,35,44,48,50,52,54,59,61,65,68,77,78,79",
      "opentime": "2018-06-20 10:19:46"
    },
    {
      "code": "xhgklb",
      "expect": "20171115714",
      "opencode": "02,09,10,12,16,19,25,33,36,46,56,58,59,60,64,67,70,72,76,80",
      "opentime": "2018-01-10 16:29:58"
    },
    {
      "code": "xjpklb",
      "expect": "2811732",
      "opencode": "05,07,13,14,16,20,23,27,31,33,34,39,49,53,55,62,70,71,78,80",
      "opentime": "2018-01-10 16:29:58"
    },
    {
      "code": "xjssc",
      "expect": "20180620002",
      "opencode": "2,6,6,7,3",
      "opentime": "2018-06-20 10:20:25"
    },
    {
      "code": "ynklsf",
      "expect": "20180620004",
      "opencode": "20,15,13,17,03,19,08,12",
      "opentime": "2018-06-20 10:16:55"
    },
    {
      "code": "zj11x5",
      "expect": "2018062011",
      "opencode": "04,05,08,11,01",
      "opentime": "2018-06-20 10:11:15"
    },
    {
      "code": "zjsyxw",
      "expect": "18062011",
      "opencode": "04,05,08,11,01",
      "opentime": "2018-06-20 10:11:35"
    }
  ]
}
```

## The following table describes each item in the response.

|Response item | Description |
|----------|------------|
| **rows** | 總資料數. |
| **remain** | 無用. |
| **{data}** | 彩種開獎內容. |
| **{data}/code** | 彩種名稱. |
| **{data}/expect** | 獎期. |
| **{data}/opencode** | 開號內容.|
| **{data}/opentime** | 開獎時間. |

## Error and status codes

The following table lists the status and error codes related to this request.

| Status code | Meaning |
|--------|----------|
| 200 | Successful response |

這版為舊版的API 所以沒有符合RESTful規範
錯誤返回 data內容為ERROR_MSG

```json
{
  "rows": null,
  "remain": "5524hrs",
  "data": "ERROR_OPENAPIACTION_GETGAMERESULT_02"
}
```

## Code example

取得抓號內容 

```php
$src = 'http://kaijiang.goodgaming.com/index.php?s=/OpenApi/getGameResult';
		
// time out 設定
$ctx = stream_context_create(array(
		'http' => array(
				'timeout' => 5 
		) 
));

// 抓號
if (!$json = @file_get_contents(urldecode($src), 0, $ctx)) {
	return false;
}

$result = json_decode($json, true);

return $result;
```
