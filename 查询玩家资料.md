# USER/{id}

查詢玩家資料

## Endpoint definition

`USER/{id}`

## HTTP method

<span class="label label-primary">GET</span>

## Parameters

| Parameter | Description | Data Type |
|-----------|-----------|-----------|
| id | 玩家編號 | integer |

## Sample request

```
curl -I -X GET "http://ggtestapi.goodgaming.com/USER/1"
```

## Sample response

```json
  {
      "id": "1",
      "user_account": "system",
      "user_password": "fc4f7d0b4dd3ed4bcaea91bf672a4e74",
      "balance_password": "fc4f7d0b4dd3ed4bcaea91bf672a4e74",
      "user_nickname": "系统账号",
      "user_email": "dada@qq.com",
      "mobile_number": "11",
      "qq_account": "564678",
      "user_level": "1",
      "last_login": "2018-07-13 12:58:42",
      "last_login_ip": "220.128.110.143",
      "continuous_login": "1",
      "last_password_edit": "2018-05-11 15:58:15",
      "create_time": "0000-00-00 00:00:00",
      "disabled_setting": "{\"login\":\"0\",\"bet\":\"0\",\"withdraw\":\"0\",\"proxy_recharge\":\"0\",\"unlink_bank\":\"1\"}",
      "finance_setting": null,
      "user_setting": null,
      "is_active": "1",
      "is_important": "0",
      "is_beta": "0",
      "google_code": "0",
      "push_token": "24a193593be408a11e43144395dbfa99",
      "balance_info": {
          "user_balance": "439329.43204",
          "balance_status": "1"
      },
      "proxy_info": {
          "id": "1",
          "user_id": "1",
          "parent_id": "0",
          "parent_tree": "[1]",
          "last_edit": "2015-08-17 14:18:51",
          "is_active": "1",
          "parent_info": []
      },
      "status_code": 1
  }
```

The following table describes each item in the response.

|Response item | Description |
|----------|------------|
| **id** | 用户id |
| **user_account** | 帐号 |
| **user_password** | 密码 |
| **balance_password** | 资金密码 |
| **user_nickname** | 用戶暱稱 |
| **user_email** | 用戶郵箱 |
| **mobile_number** | 手機號碼 |
| **qq_account** | qq帳號 |
| **user_level** | 用户级别 |
| **last_login** | 最后登入时间 |
| **last_login_ip** | 最后一次登入ip |
| **continuous_login** | 連續登入天數 |
| **last_password_edit** | 密碼最後更新時間 |
| **create_time** | 创建时间 |
| **disabled_setting** | 封鎖項目 |
| **finance_setting** | 提现上限设定 |
| **user_setting** | 使用者設定 |
| **is_active** | 是否启用 |
| **is_important** | 是否為重點觀察用戶 |
| **is_beta** | 测试帐号 |
| **google_code** | google驗證器 |
| **push_token** | 推播用token |
| **{balance_info}/user_balance** | 錢包餘額 |
| **{balance_info}/balance_status** | 錢包狀態 0: 停用 1: 啟用 |
| **{proxy_info}/id** | proxy id |
| **{proxy_info}/user_id** | 用户id |
| **{proxy_info}/parent_id** | 父節點編號 |
| **{proxy_info}/parent_tree** | 關聯 |
| **{proxy_info}/last_edit** | 最后修改时间 |
| **{proxy_info}/is_active** | 是否启用 |
| **{proxy_info}/parent_info** | 上線名稱 |
| **status_code** | 狀態碼 |

## Error and status codes

The following table lists the status and error codes related to this request.

| Status code | Meaning |
|--------|----------|
| 116 | 用户不存在 |
| 200 | 成功 |