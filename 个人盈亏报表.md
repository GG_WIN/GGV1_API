# /PERSONAL/userReport

个人盈亏报表.

## Endpoint definition

`/PERSONAL/userReport`

## HTTP method

<span class="label label-primary">GEY</span>

## Parameters

| Parameter | Description | Data Type |
|-----------|------|-----|
| start_time | 開始日期. | time |
| end_time | 結束時間. | time |

## Sample request

```
curl -I -X GET "http://ggtestapi.goodgaming.com/PERSONAL/userReport/start_time/2018-07-012/end_time/2018-07-13"
```
## Sample response

```json
{
    "parent_name": "系统账号",						
    "user_percent": "1,700",						
    "user_dailypay": {								
        "id": "34",
        "user_id": "373",
        "user_dailypay": "0",
        "dailypay_tree": "[10,0]",
        "last_edit": "2018-07-06 14:40:11",
        "is_active": "1"
    },
    "user_record": {								
        "total_recharge_balance": "0.00000",
        "total_withdraw_balance": "0.00000",
        "total_promotion_balance": "2.00000",
        "total_slot_balance": "0.00000",
        "total_bonus_balance": "0.00000",
        "total_commission_balance": "0.00000",
        "total_dailypay_balance": "0.00000",
        "total_recharge_balance_display": "0",
        "total_withdraw_balance_display": "0",
        "total_promotion_balance_display": "2",
        "total_commission_balance_display": "0",
        "total_dailypay_balance_display": "0"
    },
    "game_record": {									
        "1": {
            "total_recharge_balance": "0.00000",		
            "total_withdraw_balance": "0.00000",		
            "total_bet_balance": "0.02200",				
            "total_result_balance": "0.00000",			
            "total_result_self_balance": "0.00000",		
            "total_reward_balance": "0.00000",			
            "total_real_bet_balance": 0.022,			
            "income_balance": -0.022,					
            "total_recharge_balance_display": "0",		
            "total_withdraw_balance_display": "0",		
            "total_bet_balance_display": "0.022",     
            "total_real_bet_balance_display": "0.022",
            "total_result_self_balance_display": "0", 
            "total_reward_balance_display": "0",      
            "total_result_balance_display": "0",      
            "income_balance_display": "-0.022"        
        },                                            
        "2": {
            "total_recharge_balance": 0,
            "total_withdraw_balance": 0,
            "total_bet_balance": 0,
            "total_real_bet_balance": 0,
            "total_result_balance": 0,
            "total_result_self_balance": 0,
            "total_reward_balance": 0,
            "income_balance": 0,
            "total_recharge_balance_display": "0",
            "total_withdraw_balance_display": "0",
            "total_bet_balance_display": "0",
            "total_real_bet_balance_display": "-",
            "total_result_self_balance_display": "-",
            "total_reward_balance_display": "0",
            "total_result_balance_display": "0",
            "income_balance_display": "0"
        },
    },
    "game_record_total": {								
        "total_recharge_balance": 0,
        "total_withdraw_balance": 0,
        "total_bet_balance": 0.022,
        "total_real_bet_balance": 0.022,
        "total_result_balance": 0,
        "total_result_self_balance": 0,
        "total_reward_balance": 0,
        "income_balance": -0.022,
        "total_recharge_balance_display": "0",
        "total_withdraw_balance_display": "0",
        "total_bet_balance_display": "0.022",
        "total_real_bet_balance_display": "0.022",
        "total_result_balance_display": "0",
        "total_result_self_balance_display": "0",
        "total_reward_balance_display": "0",
        "income_balance_display": "-0.022"
    },
    "product_name_lang": {			
        "1": "彩票娱乐场",
        "2": "AG",
    },
    "status_code": 1
}

```

## The following table describes each item in the response.

|Response item | Description |
|----------|------------|
| **status_code** | status codes. |
| **parent_name** | 直屬上級帳號暱稱. |
| **user_percent** | 彩票返點組. |
| **{user_dailypay}** | 日獎資料. |
| **{user_record}** | 用戶個人平台盈虧. |
| **{game_record}** | 用戶遊戲數據. |
| **{game_record}/{遊戲id}/** | 用戶遊戲數據. |
| **{game_record}/{遊戲id}/total_recharge_balance** | 充值. |
| **{game_record}/{遊戲id}/total_withdraw_balance** | 提現. |
| **{game_record}/{遊戲id}/total_bet_balance** | 下注. |
| **{game_record}/{遊戲id}/total_result_balance** | 返點. |
| **{game_record}/{遊戲id}/total_result_self_balance** | 自身返點. |
| **{game_record}/{遊戲id}/total_reward_balance** | 派獎. |
| **{game_record}/{遊戲id}/total_real_bet_balance** | 實際下注. |
| **{game_record}/{遊戲id}/income_balance** | 盈虧. |
| **{game_record}/{遊戲id}/total_recharge_balance_display** | 充值顯示. |
| **{game_record}/{遊戲id}/total_withdraw_balance_display** | 提現顯示. |
| **{game_record}/{遊戲id}/total_bet_balance_display** | 下注顯示. |
| **{game_record}/{遊戲id}/total_real_bet_balance_display** | 返點顯示. |
| **{game_record}/{遊戲id}/total_result_self_balance_display** | 自身返點顯示. |
| **{game_record}/{遊戲id}/total_reward_balance_display** | 派獎顯示. |
| **{game_record}/{遊戲id}/total_result_balance_display** | 實際下注顯示. |
| **{game_record}/{遊戲id}/income_balance_display** | 盈虧顯示. |
| **{game_record_total}** | 總和. |
| **{game_record_total}/** | 用戶遊戲數據. |
| **{game_record_total}/total_recharge_balance** | 充值. |
| **{game_record_total}/total_withdraw_balance** | 提現. |
| **{game_record_total}/total_bet_balance** | 下注. |
| **{game_record_total}/total_result_balance** | 返點. |
| **{game_record_total}/total_result_self_balance** | 自身返點. |
| **{game_record_total}/total_reward_balance** | 派獎. |
| **{game_record_total}/total_real_bet_balance** | 實際下注. |
| **{game_record_total}/income_balance** | 盈虧. |
| **{game_record_total}/total_recharge_balance_display** | 充值顯示. |
| **{game_record_total}/total_withdraw_balance_display** | 提現顯示. |
| **{game_record_total}/total_bet_balance_display** | 下注顯示. |
| **{game_record_total}/total_real_bet_balance_display** | 返點顯示. |
| **{game_record_total}/total_result_self_balance_display** | 自身返點顯示. |
| **{game_record_total}/total_reward_balance_display** | 派獎顯示. |
| **{game_record_total}/total_result_balance_display** | 實際下注顯示. |
| **{game_record_total}/income_balance_display** | 盈虧顯示. |
| **{product_name_lang}/{遊戲id}** | 產品名稱對照表. |








## Error and status codes

The following table lists the status and error codes related to this request.

| Status code | Meaning |
|--------|----------|
| 139 | 用户在线资料错误 |
| 151 | 查询采种ID为空 |
| 152 | 无法找到该彩种 |
| 153 | 奖期资料获取失败 |
| 154 | 无当前奖期资料 |
| 155 | 该玩法已经关闭 |
| 156 | 下注内容数量检查错误 |
| 157 | 建立注单确认资讯失败 |
| 157 | 建立注单确认资讯失败 |





