# USER/delUserProxyPromo

刪除代理推廣.

## Endpoint definition

`USER/delUserProxyPromo`

## HTTP method

<span class="label label-primary">DELETE</span>

## Parameters

| Parameter | Description | Data Type |
|-----------|------|-----|
| id | 代理推廣ID. | string |



## Sample request

```
curl -I -X GET "http://ggtestapi.goodgaming.com/USER/delUserProxyPromo?id=1"
```

## Sample response
```json
{
    "status_code": 1
}
```


## The following table describes each item in the response.
 
|Response item | Description |
|----------|------------|
| **status_code** | status_code. |

## Error and status codes
 
 The following table lists the status and error codes related to this request.
 
| Status code | Meaning |
|--------|----------|
| 139 | 用户在线资料错误 |
| 821 | 沒有id |
| 822 | 取得代理資料失敗 |
| 823 | 取得代理資料失敗 |
| 824 | 玩家資訊錯誤 |
| 825 | 刪除代理資料失敗 |
| 826 | 加入追蹤紀錄 |

 
 這版為舊版的API 所以沒有符合RESTful規範
 錯誤返回 data內容為ERROR_MSG
 